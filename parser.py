from __future__ import annotations
import re
from pprint import pprint
from dataclasses import dataclass

@dataclass(init=False)
class ASTItem: ...

@dataclass(init=False)
class Root(ASTItem):
	children: list[Statement]

@dataclass(init=False)
class Statement(ASTItem): ...

@dataclass(init=False)
class Block(Statement):
	children: list[Statement]

@dataclass(init=False)
class PromiseDef(Block):
	name: str
	args: list[str]

@dataclass(init=False)
class IfStatement(Block):
	expr: str
	children: list[Statement]
	else_b: IfStatement | Block = None

@dataclass(init=False)
class WhileStatment(Statement):
	expr: str
	children: list[Statement]

@dataclass(init=False)
class VarDef(Statement):
	name: str
	val: Expression | None = None

@dataclass(init=False)
class Expression(Statement): ...

@dataclass(init=False)
class VarSet(Expression):
	name: str
	val: Expression

@dataclass(init=False)
class Operation(Expression):
	left: Expression
	right: Expression
	oper: str

@dataclass(init=False)
class Var(Expression):
	name: str

@dataclass(init=False)
class Promise(Expression):
	prom: Expression
	args: list[Expression]

@dataclass(init=False)
class Literal(Expression):
	val: int | float | None

@dataclass(init=False)
class Await(Expression):
	expr: list[tuple[Expression,bool]]

@dataclass(init=False)
class Return(Statement):
	expr: Expression

def lex(code) -> list[tuple[str, str]]:
	return [t for t in [[(k,v) for k,v in m.groupdict().items() if v is not None][0] for m in re.finditer("|".join([
		r"(?P<comment>//[^\n]*)",
		r"(?P<cmp>!=|==|[<>]=?)",
		r"(?P<keyword>await|promise|var|return|if|else|while|=|,|\(|\)|\{|\})",
		r'(?P<lit>-?[0-9]+(\.[0-9]+)?|None|\"([^"]|\\")*\")',
		r"(?P<name>[a-zA-Z_][a-zA-Z0-9_]*)",
		r"(?P<oper>[+\-*/%])",
		r"(?P<wha>\w+)"
	]), code)] if t[0] != "comment"]

escapes = {
	"\\\"": "\"",
	r"\n":  "\n",
	r"\t":  "\t",
	r"\\":  "\\",
}

class Parser:
	out: Root
	tokens: list[tuple[str, str]]
	iptr: int
	
	@classmethod
	def parse(cls, code) -> Root:
		self = cls(code)
		self.parse_root()
		return self.out

	def __init__(self, code):
		self.out = Root()
		self.out.children = []
		self.out.pos = (0,0)
		self.tokens = lex(code)
		self.iptr = 0
	
	def check(self, offset: int, typ: str = None) -> bool:
		if self.iptr + offset >= len(self.tokens):
			return False
		if typ is None:
			return True
		return self.tokens[self.iptr + offset][0] == typ
	def checkv(self, offset: int, v: str) -> bool:
		if self.iptr + offset >= len(self.tokens):
			return False
		return self.tokens[self.iptr + offset][1] == v

	def parse_root(self):
		while self.check(0):
			self.out.children.append(self.parse_statement())

	def parse_statement(self) -> Statement:
		if self.checkv(0, "promise"):
			return self.parse_promisedef()
		if self.checkv(0, "var"):
			self.iptr += 2
			out = VarDef()
			out.name = self.tokens[self.iptr-1][1]
			if self.checkv(0, "="):
				self.iptr += 1
				out.val = self.parse_expr()
			return out
		if self.checkv(0, "return"):
			self.iptr += 1
			out = Return()
			out.expr = self.parse_expr()
			return out
		if self.checkv(0, "if"):
			self.iptr += 1
			out = IfStatement()
			out.expr = self.parse_expr()
			if self.check(0, "cmp"):
				left = out.expr
				out.expr = Operation()
				out.expr.left = left
				out.expr.oper = self.tokens[self.iptr][1]
				self.iptr += 1
				out.expr.right = self.parse_expr()
			self.parse_block(out)
			out1 = out
			while self.checkv(0, "else"):
				self.iptr += 1
				if self.checkv(0, "if"):
					self.iptr += 1
					out2 = IfStatement()
					out2.expr = self.parse_expr()
					if self.check(0, "cmp"):
						left = out.expr
						out.expr = Operation()
						out.expr.left = left
						out.expr.oper = self.tokens[self.iptr][1]
						self.iptr += 1
						out.expr.right = self.parse_expr()
				else:
					out2 = Block()
				self.parse_block(out2)
				out1.else_b = out2
				out1 = out2
				if not isinstance(out1, IfStatement):
					break
			return out
		if self.checkv(0, "while"):
			self.iptr += 1
			out = WhileStatment()
			out.expr = self.parse_expr()
			if self.check(0, "cmp"):
				left = out.expr
				out.expr = Operation()
				out.expr.left = left
				out.expr.oper = self.tokens[self.iptr][1]
				self.iptr += 1
				out.expr.right = self.parse_expr()
			self.parse_block(out)
			return out

		return self.parse_expr()

	def parse_block(self, block: Block):
		if not self.checkv(0,"{"):
			raise ValueError(">:( bad code. Fix it!!!!!!")
		self.iptr += 1
		block.children = []
		while not self.checkv(0,"}"):
			block.children.append(self.parse_statement())
		self.iptr += 1

	def parse_promisedef(self) -> PromiseDef:
		self.iptr += 1
		out = PromiseDef()
		out.name = self.tokens[self.iptr][1]
		self.iptr += 1
		if not self.checkv(0,"("):
			raise ValueError(">:( bad code. Fix it!!!!!!")
		self.iptr += 1
		out.args = []
		while self.check(0, "name"):
			out.args.append(self.tokens[self.iptr][1])
			self.iptr += 1
			if self.checkv(0, ","):
				self.iptr += 1

		if not self.checkv(0,")"):
			raise ValueError(">:( bad code. Fix it!!!!!!")
		self.iptr += 1
		self.parse_block(out)
		return out

	def parse_expr(self) -> Expression:
		if not self.check(0):
			raise EOFError("hungy :(")
		if self.checkv(1, "="):
			self.iptr += 2
			out = VarSet()
			out.name = self.tokens[self.iptr-2][1]
			out.val = self.parse_expr()
			return out
		out = self.parse_oper()
		while self.checkv(0, "("):
			prom = out
			out = Promise()
			out.prom = prom
			self.iptr += 1
			out.args = []
			while not self.checkv(0, ")"):
				out.args.append(self.parse_expr())
				if self.checkv(0, ","):
					self.iptr += 1
			self.iptr += 1
		return out
	
	def parse_oper(self) -> Expression:
		if not self.check(0):
			raise EOFError("hungy :(")
		out = self.parse_loper()
		if not self.check(0):
			return out
		if self.tokens[self.iptr][0] != "oper" and self.tokens[self.iptr][1].startswith("-"):
			self.tokens.insert(self.iptr, ("oper","-"))
			self.tokens[self.iptr+1] = (self.tokens[self.iptr+1][0],self.tokens[self.iptr+1][1].removeprefix("-"))
		if self.checkv(0, "+") or self.checkv(0, "-"):
			oper = self.tokens[self.iptr][1]
			self.iptr += 1
			out1 = out
			out = Operation()
			out.left = out1
			out.oper = oper
			out.right = self.parse_loper()
		return out
	
	def parse_loper(self) -> Expression:
		if not self.check(0):
			raise EOFError("hungy :(")
		out = self.parse_val()
		if self.checkv(0, "*") or self.checkv(0, "/") or self.checkv(0, "%"):
			oper = self.tokens[self.iptr][1]
			self.iptr += 1
			out1 = out
			out = Operation()
			out.left = out1
			out.oper = oper
			out.right = self.parse_val()
		return out

	def parse_val(self) -> Expression:
		if self.check(0, "lit"):
			out = Literal()
			if self.tokens[self.iptr][1] == "None":
				out.val = None
			elif self.tokens[self.iptr][1].startswith('"'):
				out.val = self.tokens[self.iptr][1][1:-1]
				for old,new in escapes.items():
					out.val = out.val.replace(old,new)
			elif "." in self.tokens[self.iptr][1]:
				out.val = float(self.tokens[self.iptr][1])
			else:
				out.val = int(self.tokens[self.iptr][1])
			self.iptr += 1
			return out
		if self.check(0, "name"):
			name = self.tokens[self.iptr][1]
			self.iptr += 1
			out = Var()
			out.name = name
			return out
		if self.checkv(0, "await"):
			self.iptr += 1
			out = Await()
			out.expr = []
			while True:
				if self.checkv(0,"("):
					out.expr.append((self.parse_expr(),True))
				else:
					out.expr.append((self.parse_expr(),False))
				if self.checkv(0,","):
					self.iptr += 1
					continue
				break
			return out
		if self.checkv(0, "("):
			self.iptr += 1
			out = self.parse_expr()
			if not self.checkv(0, ")"):
				raise ValueError(">:( bad code. Fix it!!!!!!")
			self.iptr += 1
			return out
		raise ValueError(">:( bad code. Fix it!!!!!!")

if __name__ == "__main__":
	code = open("test.asl").read()
	pprint(lex(code))
	pprint(Parser.parse(code))
	
