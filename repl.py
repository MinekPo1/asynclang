from prompt_toolkit import PromptSession
from prompt_toolkit.history import History, InMemoryHistory
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.output import create_output

from parser import Parser, Expression
from runner import Runner, VarVal, PromiseGeneratorNative

async def repl():
	runner = Runner()

	class ReplExit(PromiseGeneratorNative):
		def __init__(self):
			pass
		@staticmethod
		async def corog():
			exit()
		def __repr__(self):
			return "Use await exit() or Ctrl-D (i.e. EOF) to exit."

	class ReadonlyHistory(History):
		other: History
		def __init__(self, other: History):
			self.other = other
			super().__init__()

		def get_strings(self):
			return self.other.get_strings()
		async def load(self):
			async for v in self.other.load():
				yield v
		def load_history_strings(self):
			yield from self.other.load_history_strings()
		def store_string(self, _):
			pass
	
	history = InMemoryHistory()
	kbds = KeyBindings()
	@kbds.add("tab")
	def _(event):
		prompt.default_buffer.insert_text("\t")
	@kbds.add("}")
	def _(event):
		buf = prompt.default_buffer.text
		bufl = buf.splitlines()
		if bufl[-1].strip():
			prompt.default_buffer.insert_text("}")
			return
		bufl[-1] = "\t"*(buf.count("{") - buf.count("}") - 1) + "}"
		prompt.default_buffer.text = "\n".join(bufl)

	out = create_output()
	out._write = out.write
	out._write_raw = out.write_raw
	out.write = lambda t: out._write(t.replace("^I","\t").expandtabs(2))
	out.write_raw = lambda t: out._write_raw(t.replace("^I","\t").expandtabs(2))

	prompt = PromptSession(
		message=">>> ",prompt_continuation = lambda _, __, ___: "... ",
		history=ReadonlyHistory(history), key_bindings=kbds, output=out
	)

	runner.def_var("exit", ReplExit())
	runner.def_var("_")


	while True:
		code = ""
		try:
			while code == "" or code.count('{') > code.count('}'):
				if code:
					for _ in code.splitlines():
						print("\033[A",end="")
					code += "\n" + "\t"*(code.count('{')-code.count('}'))
				code = await prompt.prompt_async(default=code)
			history.append_string(code)
		except EOFError:
			break
		except KeyboardInterrupt:
			continue
		try:
			linetree = Parser.parse(code)
			if isinstance(linetree.children[-1], Expression):
				for i in linetree.children:
					v = await runner.evaluate(i)
				runner.set_var("_", v)
				if v is not None:
					print(v)
				continue
			for i in linetree.children:
				v = await runner.execute(i)
			runner.set_var("_",None)
		except Exception as ex:
			print(ex.__class__.__name__+":",ex)

