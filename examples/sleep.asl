await sleep
await print

promise sleepy(n) {
	await sleep(1)
	return await n
}
await sleepy
promise not_sleepy(n) {
	return await n
}
await not_sleepy

var a
var b
a = await     sleepy(1)
a = await not_sleepy(2)
b = await sleepy(await     sleepy(1))
b = await sleepy(await not_sleepy(2))

await print(await a, await b)
