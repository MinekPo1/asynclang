from parser import Parser, Expression
from runner import Runner, VarVal, PromiseGeneratorNative
import asyncio
import argparse
from sys import argv, stdin
from repl import repl

from warnings import filterwarnings
parser = argparse.ArgumentParser("asynclang")
parser.add_argument("inp", nargs="?")

class ParserRes:
	inp: str | None

async def main(*args):
	args = parser.parse_args(args, ParserRes())
	filterwarnings(action="ignore", category=RuntimeWarning)
	
	if args.inp is None:
		if stdin.isatty():
			await repl()
			return
		code = stdin.read()
	else:
		code = open(args.inp).read()
	
	runner = Runner()

	try:
		tree = Parser.parse(code)
		for i in tree.children:
			await runner.execute(i)
	except Exception as ex:
		print(ex.__class__.__name__+":",ex)

if __name__ == "__main__":
	asyncio.run(main(*argv[1:]))
