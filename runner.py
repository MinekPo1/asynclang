from __future__ import annotations
from typing import Awaitable, Any, Callable
from parser import *
import asyncio

class PromiseGenerator:
	promdef: PromiseDef
	runner: Runner

	def __bool__(self):
		return False

	def __repr__(self):
		return "<PromiseGenerator>"

class PromiseGeneratorNative:
	corog: Any

	def __init__(self, corog):
		self.corog = corog

	def __bool__(self):
		return False

	def __repr__(self):
		return "<PromiseGeneratorNative>"

class PromiseVal:
	children: list[Statement]
	done: bool = False
	val: ValT = None
	runner: Runner
	
	def __bool__(self):
		return self.done

	def __repr__(self):
		return "<Promise>"

class PromiseValNative:
	coro: Awaitable[[], ValT]
	done: bool = False
	val: ValT = None
	
	def __init__(self, coro):
		try:
			self.coro = coro()
		except TypeError:
			self.coro = coro
	
	def __bool__(self):
		return self.done
	
	def __repr__(self):
		return "<PromiseNative>"

@dataclass(init=False)
class VarVal:
	val: ValT = None
	coros: list[Awaitable[[], None]]
	defined: bool = False

ValT = int | float | str | PromiseVal | PromiseValNative | PromiseGenerator | PromiseGeneratorNative | None

class Runner:
	var_dict: dict[str, VarVal]
	super_runner: Runner | None = None
	return_hook: Callable[[ValT], None] = None

	def __init__(self, *, super_runner = None, no_builtins = False):
		self.var_dict = {}
		self.super_runner = super_runner
		if no_builtins or super_runner is not None:
			return
		for k,v in bultins.items():
			self.def_var(k,v)

	def get_var(self, name: str) -> ValT:
		runner = self
		while runner is not None:
			if name in runner.var_dict and runner.var_dict[name].defined:
				return runner.var_dict[name].val
			runner = runner.super_runner
		raise RuntimeError(f"Undefined var `{name}`!")

	def set_var(self, name: str, val: ValT):
		runner = self
		while runner is not None:
			if name in runner.var_dict and runner.var_dict[name].defined:
				async def set_coro():
					runner.var_dict[name].val = val
				runner.var_dict[name].coros.append(set_coro())
				return
			runner = runner.super_runner
		runner = self
		while runner is not None:
			if name in runner.var_dict:
				async def set_coro():
					runner.var_dict[name].val = val
				runner.var_dict[name].coros.append(set_coro())
				return
			runner = runner.super_runner
		raise RuntimeError(f"Undefined var `{name}`!")

	async def await_var(self, name: str):
		runner = self
		while runner is not None:
			if name in runner.var_dict:
				await asyncio.gather(*runner.var_dict[name].coros)
				return runner.var_dict[name].val
			runner = runner.super_runner
		raise RuntimeError(f"Undefined var `{name}`!")

	def def_var(self, name: str, val: ValT = None):
		if name in self.var_dict:
			raise ValueError("Done already duh")
		self.var_dict[name] = VarVal()
		async def define():
			self.var_dict[name].defined = True
		self.var_dict[name].coros = [define()]
		self.var_dict[name].val = val

	async def evaluate(self, expr: Expression) -> ValT:
		if isinstance(expr, Literal):
			return expr.val
		if isinstance(expr, Var):
			return self.get_var(expr.name)
		if isinstance(expr, Operation):
			return self.evaluate_oper(expr)
		if isinstance(expr, Promise):
			return await self.evaluate_promise(expr)
		if isinstance(expr, VarSet):
			return await self.evaluate_varset(expr)
		if isinstance(expr, Await):
			return await self.evaluate_await(expr)
		raise TypeError(f"what {expr}")
	
	def evaluate_oper(self, oper: Operation) -> ValT:
		if oper.oper == "+":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return lval + rval
		if oper.oper == "-":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return lval - rval
		if oper.oper == "*":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return lval * rval
		if oper.oper == "/":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return lval / rval
		if oper.oper == "%":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return lval % rval
		if oper.oper == "==":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return 1 if lval == rval else 0
		if oper.oper == "!=":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return 1 if lval != rval else 0
		if oper.oper == "<":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return 1 if lval <  rval else 0
		if oper.oper == "<=":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return 1 if lval <= rval else 0
		if oper.oper == ">":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return 1 if lval >  rval else 0
		if oper.oper == ">=":
			@PromiseValNative
			async def out():
				lval = await self.evaluate(oper.left)
				rval = await self.evaluate(oper.right)
				return 1 if lval >= rval else 0
		return out

	async def evaluate_promise(self, prom: Promise) -> PromiseVal | PromiseValNative:
		promg = await self.evaluate(prom.prom)
		if not isinstance(promg,(PromiseGenerator, PromiseGeneratorNative)):
			raise TypeError("*cries*")
		if isinstance(promg, PromiseGenerator):
			out = PromiseVal()
			if len(promg.promdef.args) != len(prom.args):
				raise ValueError("Please lern to count :(")
			args = {}
			async def define(k,v):
				args[k].defined = True
				args[k].val = await self.evaluate(v)

			for k,v in dict(zip(promg.promdef.args, prom.args)).items():
				args[k] = VarVal()
				args[k].coros = [define(k,v)]
			out.children = promg.promdef.children
			out.runner = Runner(super_runner=promg.runner)
			out.runner.var_dict.update(args)
			return out
		return PromiseValNative(promg.corog(*[await self.evaluate(i) for i in prom.args]))

	async def evaluate_varset(self, varset: VarSet) -> ValT:
		if varset.name not in self.var_dict:
			raise RuntimeError("Undefined var!")

		@PromiseValNative
		async def out():
			return await self.evaluate(varset.val)

		async def set_task():
			if out.done:
				val = out.val
			else:
				val = await out.coro
			if not self.var_dict[varset.name].defined:
				raise RuntimeError("Undefined var!")
			self.var_dict[varset.name].val = val
		self.var_dict[varset.name].coros.append(set_task())

		return out
	async def evaluate_await(self, await_: Await) -> ValT:
		async def await_item(expr, paren):
			if isinstance(expr, Var) and not paren:
				if expr.name not in self.var_dict:
					raise RuntimeError("Undefined var!")

				var = self.var_dict[expr.name]

				await asyncio.gather(*var.coros)
				var.coros.clear()
				return var.val
			val = await self.evaluate(expr)
			
			if isinstance(val, PromiseValNative):
				if not val.done:
					val.done = True
					return await val.coro
				return val.val
			if isinstance(val, PromiseVal):
				ret: ValT = None
				def hook(v: ValT):
					nonlocal ret
					ret = v
				subrunner = val.runner
					
				subrunner.return_hook = hook
				for i in val.children:
					await subrunner.execute(i)
				return ret
			raise TypeError("huhh??")
		return (await asyncio.gather(*[await_item(expr, paren) for expr, paren in await_.expr]))[-1]
	
	async def execute(self, statement: Statement) -> None:
		if isinstance(statement, Expression):
			await self.evaluate(statement)
			return

		if isinstance(statement, VarDef):
			self.def_var(statement.name)
			async def set_var():
				self.set_var(statement.name, await self.evaluate(statement.val))
			self.var_dict[statement.name].coros.append(set_var())
			return
		
		if isinstance(statement, PromiseDef):
			pg = PromiseGenerator()
			pg.promdef = statement
			pg.runner = self
			self.def_var(statement.name, pg)
			return
		
		if isinstance(statement, IfStatement):
			if await self.evaluate(statement.expr):
				await self.execute_block(statement)
			elif statement.else_b is not None:
				await self.execute(statement.else_b)
			return
		
		if isinstance(statement, WhileStatment):
			while await self.evaluate(statement.expr):
				await self.execute_block(statement)
			return
		
		if isinstance(statement, Block):
			self.execute_block(statement)
		
		if isinstance(statement, Return):
			val = await self.evaluate(statement.expr)
			if self.return_hook is None:
				raise ValueError("Illegalll!!!!")
			self.return_hook(val)
			return

	async def execute_block(self, block: Block):
		for i in block.children:
			await self.execute(i)

bultins: dict[str, PromiseGeneratorNative] = {}

def builtin(promg: PromiseGeneratorNative):
	bultins[promg.corog.__name__.removeprefix("builtin_")] = promg

@builtin
@PromiseGeneratorNative
async def builtin_print(*args):
	print(*args)

@builtin
@PromiseGeneratorNative
async def builtin_input():
	return input()

@builtin
@PromiseGeneratorNative
async def builtin_sleep(time):
	await asyncio.sleep(time)
